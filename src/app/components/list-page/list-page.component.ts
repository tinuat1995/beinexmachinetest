import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/app/service/common.service';


@Component({
  selector: 'app-list-page',
  templateUrl: './list-page.component.html',
  styleUrls: ['./list-page.component.css']
})
export class ListPageComponent implements OnInit {
  public doughnutChartLabels: string[] = ['Male', 'Female'];
  public doughnutChartData: number[]
  chartOptions = {
    responsive: true
  };
  colorScheme = {
    domain: ["#5AA454"]
  };
  tabledata = [
    { id : 1 , no: 1 , name : 'John doe', gender : 'Male', email: 'John@gmail.com' , phone : 9867998657, message : 'hello'},
    { id : 2 , no: 2 , name : 'Adam Jhone', gender : 'Male', email: 'Adam@gmail.com' , phone :9876538978, message : 'how are you'},
    { id : 3 , no: 3 , name : 'Emilia Clark', gender : 'Female', email: 'Emilia@gmail.com' , phone : 7800875689, message : 'hai'},
    { id : 4 , no: 4 , name : 'Tony Stark', gender : 'Male', email: 'Tony@gmail.com' , phone : 6899778998, message : 'demo message '},
    { id : 5 , no: 5 , name : 'Mariya Dezuza', gender : 'Female', email: 'Mariya@gmail.com' , phone : 7878786787, message : 'sample'},
  ]
  column: string = "CategoryName";
  newData: any;
  maleCount: number = 0;
  femaleCount: number = 0;
  percentageMen: any;
  percentageWomen: number;

  constructor( private service : CommonService) { }

  ngOnInit(): void {
    this.service.currentNumber.subscribe(data=>{
      this.newData = data
      if(this.newData != ''){
        this.tabledata.push(this.newData)
      }
      
    })
    
    this.calculate()
  }

  calculate(){
    let length = this.tabledata.length
    var arrayCount = 0;
    this.tabledata.map(data=>{
      if(data.gender=='Male'){
        this.maleCount = this.maleCount + 1
      }else if(data.gender=='Female'){
      this.femaleCount = this.femaleCount + 1
      }
      if(data.id!=0){
        arrayCount++
      }else{
        arrayCount++
        data['no'] = arrayCount
      }
    })
    this.percentageMen = this.maleCount/length * 100;
    this.percentageWomen = this.femaleCount/length * 100;
    this.doughnutChartData = [this.percentageMen,this.percentageWomen];
    this.sortData();
  }

  sortData(){
    this.tabledata.sort(function(a,b){
      if ( a.name < b.name ){
        return -1;
      }
      if ( a.name > b.name ){
        return 1;
      }
      return 0;
    })
  }

}
