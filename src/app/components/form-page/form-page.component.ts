import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../service/common.service'
declare const $ :any
@Component({
  selector: 'app-form-page',
  templateUrl: './form-page.component.html',
  styleUrls: ['./form-page.component.css']
})
export class FormPageComponent implements OnInit {
name;
message;
phone;
email;
male;
female;
  namechk: boolean = false;
  genderChk: boolean = false;
  emailChk: boolean = false;
  phoneChk: boolean = false;
  gender: any;
  status: boolean;
  genders: any;
  emailErrormsg: string;

  constructor( private service : CommonService) { }

  ngOnInit(): void {
  }

  submitForm(){
    if(this.name == undefined){
      this.namechk = true;
    }else if(this.gender == undefined ){
      console.log("male",this.male)
     this.genderChk = true
    }else if(this.email == undefined){
      this.emailChk = true
      this.emailErrormsg = "Email is Required*"
    }else if(!this.validateEmail()){
      this.emailChk = true;
      this.emailErrormsg = "Enter a valid email"
    }
    else if (this.phone == undefined){
      this.phoneChk = true
    }else{
     var  obj = new Object
         obj['id'] = 0;
         obj['no'] = 0;
         obj['name'] = this.name,
         obj['gender'] = this.gender,
         obj['email'] = this.email,
         obj['phone'] = this.phone,
         obj['message'] = this.message
      this.service.setAmount(obj)
      this.status = true

      setTimeout(() => {
        this.status = false
      }, 2000);
     this.clear();     
    }
  }

  maleChange(value){
    this.gender = value.target.value
  }

  femaleChange(value){
     this.gender = value.target.value
  }

  validateEmail(){
    var re = /\S+@\S+\.\S+/;
    this.emailChk = !re.test(this.email)
    return  re.test(this.email)
  }

  numberValidation(event: any){
    const pattern = /^[Z0-9]*$/;   
    if (!pattern.test(event.target.value)) {
      event.target.value = event.target.value.replace(/[^Z0-9]/g, "");
    }
  }
  


  clear(){
    this.name = '';
    this.gender = '';
    this.email = '';
    this.phone = '';
    this.message = '';
    this.namechk = false;
    this.genderChk = false;
    this.emailChk = false;
    this.phoneChk = false;
    this.genders = null
  }

}
