import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  public amt = new BehaviorSubject('');
  currentNumber : any = this.amt.asObservable()
  constructor() { }

  setAmount(amount){
    this.amt.next(amount)
  }
}
